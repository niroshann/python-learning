menu = []
menu.append(["egg", "spam", "bacon"])
menu.append(["egg", "sausage", "bacon"])
menu.append(["egg", "spam"])
menu.append(["egg", "bacon", "spam"])
menu.append(["egg", "bacon", "sausage", "spam"])
menu.append(["spam", "bacon", "sausage", "spam"])
menu.append(["spam", "egg", "spam", "spam", "bacon", "spam"])
menu.append(["spam", "egg", "sausage", "spam"])

# my solution
for meal in menu:
    if not "spam" in meal:
        print(meal)
        # my solution
        # for i in range(len(meal)):
        #     print(meal[i])
        #
        #  Tim's simple and elegant solution
        for ingrediants in meal:
            print(ingrediants)