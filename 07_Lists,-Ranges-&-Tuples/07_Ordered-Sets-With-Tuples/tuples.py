# TUPLE is a immutable object whereas LIST is a mutable object

# t = "a", "b", "c"
# print(t)
#
# print("a", "b", "c")
# print(("a", "b", "c"))


# welcome = "Welcome to my Nightmare", "Alice Cooper", 1975
# bad = "Bad Company" "Bad Company", 1974
# budgie = "Nightflight", "Budgie", 1981
# imelda = "More Mayhem", "Imilda May", 2011
# metallica = "Ride the Lightning", "Metallica", 1984
#
# metallica2 = ["Ride the Lightning", "Metallica", 1984]
# print(metallica2)
#
# # this is called unpacking the tuple
# title, artist, year = imelda
# print(title)
# print(artist)
# print(year)


# More on Ordered sets with TUPLES
#
# welcome = "Welcome to my Nightmare", "Alice Cooper", 1975
# bad = "Bad Company", 1974
# budgie = "Nightflight", "Budgie", 1981
# imelda = "More Mayhem", "Imelda May", 2011, 1, "Pulling the Rug", 2, "Psycho", 3, "Mayhem", 4, "Kentish Town Waltz"
#
# print(imelda)
#
# title, artist, year, track1, track2, track3, track4 = imelda
# print(title)
# print(artist)
# print(year)
# print(track1)
# print(track2)
# print(track3)
# print(track4)


for song in tracks:
    track, title = song
    print("\tTrack number {}, Title: {}".format(track, title))