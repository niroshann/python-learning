myList = ["a", "b", "c", "d"]

# text also can be added as below
# newString = "someRandomText ".join(myList)
newString = ",".join(myList)
print(newString)
