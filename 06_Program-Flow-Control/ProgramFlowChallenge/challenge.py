print("Please guess a desired IP address : ")
userEnteredIpAddress = input()

print("this is the IP address you've entered " + userEnteredIpAddress)

counter = 1
for digits in userEnteredIpAddress:
    if digits == '.':
        counter += 1
        continue
    print("number is " + digits)

print("available segments {0}".format(counter))
