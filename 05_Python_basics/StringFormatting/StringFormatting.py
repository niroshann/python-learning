age = 24
print("My age is " + str(age) + " years")

print("======================# Replacement field example.=======================")
print("My age is {0} days in {1},{2},{3},{4},{5},{6} and {7}".format(31, "January", "March", "May", "July", "August", "October", "December"))

print("======================# multiple replace filed example.=======================")
print("""January: {2}
February: {0}
March: {2}
April: {2}
May: {2}
June: {2}
July: {2}
August: {2}
September: {2}
October: {2}
November: {2}
December: {2}""".format(28, 30, 31))

print("======================# Python2 deprecated way.=======================")

# %d is for integer, #s is for string.
# number refers to spacing %2

print("My age is %d years" % age)
print("My age is %d %s, %d %s" %(age, "years", 6, "months"))

for i in range(1, 12):
    print("No, %2d squared is %4d and cubed is %4d" %(i, i ** 2, i ** 3))

# number precision
print("Pi is approximately %12.50f" % (22/7))

print("======================# Python3 way.===================================")
# '<' is left justifying.
for i in range(1, 12):
    print("No, {0:2} squared is {1:<4} and cubed is {2:<4}".format(i, i ** 2, i ** 3))
